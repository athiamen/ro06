import math


# Fonction qui permet de faire les tournées basique en respectant le cout max
def initialisation_simple(sommets, distances, profits, cout_max):
    tournées = {}
    cout = 0
    tournée = []
    for i in range(1, len(sommets)-1):
        
        print(sommets[i], profits[i], cout + distances[0][i] + distances[i][len(sommets)-1])
        if cout + distances[0][i] + distances[i][len(sommets)-1] <= cout_max:
            cout += distances[0][i] + distances[i][len(sommets)-1]
            tournées[i] = [sommets[i], profits[i], cout]
            cout = 0

        else:
            cout = 0
    return tournées




def main():

    # Déclaration des sommets
    nb_sommets = int(input("Entrez le nombre de sommets : "))
    sommets = [0] * (nb_sommets+2)
    sommets[0]= "d"
    sommets[len(sommets)-1]= "a"
    # Remplissage de la liste des sommets entre d et a
    for i in range(1, nb_sommets+1):
        sommets[i] = i
    # Affichage des sommets
    print("Sommets :", sommets)
    
    # Déclaration de la materice des distances
    """distances = [[0 for i in range(nb_sommets+2)] for j in range(nb_sommets+2)]
    for i in range(0, nb_sommets+2):
        for j in range(0, nb_sommets+2):
            if i == j:
                distances[i][j] = 0
            else:
                distances[i][j] = input(f"Entrez la distance entre {sommets[i]} et {sommets[j]} : ")
    # Affichage de la matrice des distances
    print("Matrice des distances :" , distances)"""

    distances = [[0, 3, 6, 7, 8, 9, 10],
                 [3, 0, 4, 5, 6, 7, 8],
                 [6, 4, 0, 3, 4, 5, 6],
                 [7, 5, 3, 0, 5, 6, 7],
                 [8, 6, 4, 5, 0, 8, 9],
                 [9, 7, 5, 6, 8, 0, 10],
                 [10, 8, 6, 7, 9, 10, 0]]

    # Déclaration des profits5
    """profits = [0]*(nb_sommets)
    for i in range(1, nb_sommets+1):
        profits[i] = input(f"Entrez le profit de {sommets[i]} : ")
    # Affichage des profits
    print("Profits :", profits)"""
    profits = [0, 10, 15, 20, 30, 25, 0]

    # Déclaration de la limite de la capacité
    cout_max = int(input("Entrez la cout max : "))
    # Affichage de la capacité
    print("Coût total :", cout_max)

    # Initialisation des tournées
    tournées = initialisation_simple(sommets, distances, profits, cout_max)
    # Affichage des tournées
    print("Tournées :" , tournées)


if __name__ == "__main__":
    main()



import numpy as np
import random
from collections import deque


def initialisation_simple(sommets, distances, profits, cout_max):
    tournées = []
    cout = 0
    tournée = []
    for i in range(1, len(sommets)-1):
        if cout + distances[0][i] + distances[i][len(sommets)-1] <= cout_max:
            cout += distances[0][i] + distances[i][len(sommets)-1]
            # ajouter le client à la tournée
            tournées.append({'id': sommets[i], 'time': distances[0][i] + distances[i][len(sommets)-1], 'profit': profits[i]})
            cout = 0
        else:
            cout = 0
            tournées.append(tournée)
            tournée = []
    return tournées

def heuristique_gloutonne(clients, temps_max, nombre_de_vehicules):
    tournees = []
    clients_restants = clients.copy()

    for vehicule in range(nombre_de_vehicules):
        temps_courant = 0
        profit_total = 0
        tournee = ['d']
    

        while clients_restants and temps_courant < temps_max:
            # Sélectionner le client avec le meilleur ratio profit/temps
            client_selectionne = None
            meilleur_ratio = 0
            for client in clients_restants:
                if temps_courant + client['time'] <= temps_max:
                    ratio = client['profit'] / client['time']
                    if ratio > meilleur_ratio:
                        meilleur_ratio = ratio
                        client_selectionne = client

            if client_selectionne is None:
                break

            # Ajouter le client à la tournée
            tournee.append(client_selectionne['id'])
            clients_restants.remove(client_selectionne)
            temps_courant += client_selectionne['time']
            profit_total += client_selectionne['profit']

        # ajouter l'arrivée à la tournee effectuée
        tournee.append('a')
        tournees.append({'vehicule': vehicule + 1, 'tournee': tournee, 'profit': profit_total})

    return tournees

# Amélioration avec l'heuristique Tabou
def heuristique_tabou(clients, temps_max, nombre_de_vehicules, iterations=100, tabou_size=10):
    meilleure_solution = heuristique_gloutonne(clients, temps_max, nombre_de_vehicules)
    meilleure_profit = sum([tournee['profit'] for tournee in meilleure_solution])
    tabou_list = deque(maxlen=tabou_size)

    for _ in range(iterations):
        voisins = generer_voisins(meilleure_solution, clients, temps_max)
        meilleure_voisin = None
        meilleur_profit_voisin = 0

        for voisin in voisins:
            profit_voisin = sum([tournee['profit'] for tournee in voisin])
            if voisin not in tabou_list and profit_voisin > meilleur_profit_voisin:
                meilleure_voisin = voisin
                meilleur_profit_voisin = profit_voisin

        if meilleure_voisin is not None and meilleur_profit_voisin > meilleure_profit:
            meilleure_solution = meilleure_voisin
            meilleure_profit = meilleur_profit_voisin
            tabou_list.append(meilleure_voisin)

    return meilleure_solution

def generer_voisins(solution, clients, temps_max):
    # Implémentation de la génération des voisins
    voisins = []
    # Logique pour générer des voisins à partir de la solution actuelle
    # Chaque tournée commence par 'd' et se termine par 'a'
    for tournee in solution:
        for i in range(1, len(tournee['tournee']) - 1):
            for j in range(i + 1, len(tournee['tournee']) - 1):
                voisin = [t.copy() for t in solution]
                voisin_tournee = voisin[tournee['vehicule'] - 1]['tournee']
                voisin_tournee[i], voisin_tournee[j] = voisin_tournee[j], voisin_tournee[i]
                voisins.append(voisin)
    return voisins

# Amélioration avec la méthode génétique
def methode_genetique(clients, temps_max, nombre_de_vehicules, population_size=20, generations=50, mutation_rate=0.2):
    population = [heuristique_gloutonne(clients, temps_max, nombre_de_vehicules) for _ in range(population_size)]
    meilleure_solution = max(population, key=lambda x: sum([tournee['profit'] for tournee in x]))

    for _ in range(generations):
        nouvelle_population = []

        # Sélection et croisement
        for _ in range(population_size // 2):
            parent1, parent2 = random.sample(population, 2)
            enfant1, enfant2 = croisement(parent1, parent2)
            nouvelle_population.extend([enfant1, enfant2])

        # Mutation
        for individu in nouvelle_population:
            if random.random() < mutation_rate:
                mutation(individu, clients, temps_max)

        # Mise à jour de la population
        population = nouvelle_population
        meilleure_solution = max(population, key=lambda x: sum([tournee['profit'] for tournee in x]))

    return meilleure_solution

def croisement(parent1, parent2):
    enfant1 = parent1.copy()
    enfant2 = parent2.copy()
    for i in range(len(parent1)):
        if random.random() < 0.5:
            enfant1[i], enfant2[i] = parent2[i], parent1[i]
    return enfant1, enfant2

def mutation(individu, clients, temps_max):
    for tournee in individu:
            if len(tournee['tournee']) > 2:  # Assurez-vous qu'il y a des clients à muter
                index = random.randint(1, len(tournee['tournee']) - 2)  # Ne pas muter 'd' ou 'a'
                client_id = tournee['tournee'].pop(index)
                clients_restants = [c for c in clients if c['id'] == client_id]
                if clients_restants and random.random() < 0.5:
                    # Insérer le client à une nouvelle position aléatoire entre 'd' et 'a'
                    new_index = random.randint(1, len(tournee['tournee']) - 1)
                    tournee['tournee'].insert(new_index, clients_restants[0]['id'])

def main():
    # Déclaration des sommets
    nb_sommets = int(input("Entrez le nombre de sommets (7 de préférence) : ")) # nombre de sommets 7
    sommets = [0] * (nb_sommets)
    sommets[0]= "d"
    sommets[-1]= "a"
    # Remplissage de la liste des sommets entre d et a
    for i in range(1, nb_sommets-1):
        sommets[i] = i
    # Affichage des sommets
    print("Sommets :", sommets)
    
    # Déclaration de la materice des distances
    """distances = [[0 for i in range(nb_sommets+2)] for j in range(nb_sommets+2)]
    for i in range(0, nb_sommets+2):
        for j in range(0, nb_sommets+2):
            if i == j:
                distances[i][j] = 0
            else:
                distances[i][j] = input(f"Entrez la distance entre {sommets[i]} et {sommets[j]} : ")
    # Affichage de la matrice des distances
    print("Matrice des distances :" , distances)"""

    distances = [[0, 3, 6, 7, 8, 9, 10],# client d
                 [3, 0, 4, 5, 6, 7, 8], # client 1
                 [6, 4, 0, 3, 4, 5, 6], # client 2
                 [7, 5, 3, 0, 5, 6, 7], # client 3
                 [8, 6, 4, 5, 0, 8, 9], # client 4
                 [9, 7, 5, 6, 8, 0, 10], # client 5
                 [10, 8, 6, 7, 9, 10, 0]] # client a

    # Déclaration des profits5
    """profits = [0]*(nb_sommets)
    for i in range(1, nb_sommets+1):
        profits[i] = input(f"Entrez le profit de {sommets[i]} : ")
    # Affichage des profits
    print("Profits :", profits)"""
    profits = [0, 10, 15, 20, 30, 25, 0]

    print("Profits associés a chaque client :\n")
    for i in range(1, len(sommets)-1):
        print(f"Profit du Client {i}: {profits[i]}")

    # Déclaration de la limite de la capacité
    cout_max = int(input("\n\nEntrez la cout max : "))
    # Affichage de la capacité
    print("\n\nCoût total :", cout_max)

    # Initialisation des tournées
    tournées = initialisation_simple(sommets, distances, profits, cout_max)
    # Affichage des tournées
    #print("Tournées :" , tournées)
    nombre_de_vehicules = 2

    print("\n\nSolution gloutonne :\n\n")
    solution_gloutonne = heuristique_gloutonne(tournées, cout_max, nombre_de_vehicules)
    print("Solution gloutonne :", solution_gloutonne)

    print("\n\nSolution tabou :\n\n")

    solution_tabou = heuristique_tabou(tournées, cout_max, nombre_de_vehicules)
    print("Solution tabou :", solution_tabou)

    print("\n\nSolution génétique :\n\n")

    solution_genetique = methode_genetique(tournées, cout_max, nombre_de_vehicules)
    print("Solution génétique :", solution_genetique)


if __name__ == "__main__":
    main()
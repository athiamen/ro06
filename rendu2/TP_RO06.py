import numpy as np
import random
from collections import deque

def calculer_distance_tournee(tournee, sommets, distances):
    # Calculer la distance totale d'une tournée
    if len(tournee) == 2:  # Seulement 'd' et un client unique
        client_index = sommets.index(tournee[1])
        distance_totale = distances[0][client_index] + distances[client_index][-1]
    elif len(tournee) > 2:  # Plusieurs clients dans la tournée
        # Distance du dépôt au premier client
        distance_totale = distances[0][sommets.index(tournee[1])]
        
        # Somme des distances entre clients successifs
        for i in range(1, len(tournee) - 2):
            distance_totale += distances[sommets.index(tournee[i])][sommets.index(tournee[i + 1])]

        # Ajouter la distance entre le dernier client visité et l'arrivée
        distance_totale += distances[sommets.index(tournee[-2])][-1]
    else:
        distance_totale = 0  # Cas où la tournée est vide ou incorrecte

    return distance_totale



# Heuristique gloutonne
def heuristique_gloutonne(clients, sommets, distances, cout_max, nombre_de_vehicules):
    tournees = []
    clients_restants = clients.copy()

    for vehicule in range(nombre_de_vehicules):
        tournee = [{'id': 'd', 'cout': 0, 'profit': 0}]  # La tournée commence par le dépôt
        profit_total = 0
        cout_total = 0

        while clients_restants and cout_total < cout_max:
            client_selectionne = None
            meilleur_ratio = 0

            for client in clients_restants:
                client_index = sommets.index(client['id'])
                if len(tournee) == 1:  # Premier client
                    cout_client = distances['d'][client['id']] + distances[client['id']]['a']
                else:  # Client successif
                    cout_client = distances[tournee[-1]['id']][client['id']]
                # Vérifier si le client peut être ajouté à la tournée en ajoutant le coût de retour
                if cout_total + cout_client + distances[client['id']]['a'] <= cout_max:
                    ratio = client['profit'] / (cout_client + 1e-6)
                    if ratio > meilleur_ratio:
                        meilleur_ratio = ratio
                        client_selectionne = client                

            if client_selectionne is None:
                break  # Terminer la boucle si aucun client ne peut être ajouté

            # Ajouter le client sélectionné
            client_index = sommets.index(client_selectionne['id'])
            if len(tournee) == 1:  # Premier client
                cout_client = distances['d'][client_selectionne['id']]
            else:  # Client successif
                cout_client = distances[tournee[-1]['id']][client_selectionne['id']]

            tournee.append({'id': client_selectionne['id'], 'cout': cout_client, 'profit': client_selectionne['profit']})
            profit_total += client_selectionne['profit']
            cout_total += cout_client
            clients_restants.remove(client_selectionne)
            

        # Ajouter la distance finale de la tournée
        if len(tournee) > 1:
            dernier_client_index = sommets.index(tournee[-1]['id'])
            cout_retour = distances[tournee[-1]['id']]['a']
            tournee.append({'id': 'a', 'cout': cout_retour, 'profit': 0})
            cout_total += cout_retour
        else:
            tournee.append({'id': 'a', 'cout': 0, 'profit': 0})

        # Ajouter la tournée complétée
        tournees.append({
            'vehicule': vehicule + 1,
            'tournee': tournee,
            'profit': profit_total,
            'distance': cout_total
        })

    return tournees




# Amélioration avec l'heuristique Tabou
def heuristique_tabou(clients,  sommets, distances, temps_max, nombre_de_vehicules, iterations=100, tabou_size=10):
    meilleure_solution = heuristique_gloutonne(clients, sommets, distances, temps_max, nombre_de_vehicules)
    meilleure_profit = sum([tournee['profit'] for tournee in meilleure_solution])
    tabou_list = deque(maxlen=tabou_size)

    for _ in range(iterations):
        voisins = generer_voisins(meilleure_solution)
        meilleure_voisin = None
        meilleur_profit_voisin = 0

        for voisin in voisins:
            profit_voisin = sum([tournee['profit'] for tournee in voisin])
            if voisin not in tabou_list and profit_voisin > meilleur_profit_voisin:
                meilleure_voisin = voisin
                meilleur_profit_voisin = profit_voisin

        if meilleure_voisin is not None and meilleur_profit_voisin > meilleure_profit:
            meilleure_solution = meilleure_voisin
            meilleure_profit = meilleur_profit_voisin
            tabou_list.append(meilleure_voisin)

    return meilleure_solution

def generer_voisins(solution):
    # Implémentation de la génération des voisins
    voisins = []
    # Logique pour générer des voisins à partir de la solution actuelle
    # Chaque tournée commence par 'd' et se termine par 'a'
    for tournee in solution:
        for i in range(1, len(tournee['tournee']) - 1):
            for j in range(i + 1, len(tournee['tournee']) - 1):
                voisin = [t.copy() for t in solution]
                voisin_tournee = voisin[tournee['vehicule'] - 1]['tournee']
                voisin_tournee[i], voisin_tournee[j] = voisin_tournee[j], voisin_tournee[i]
                voisins.append(voisin)
    return voisins


def croisement(parent1, parent2):
    # Implémentation du croisement
    enfant1 = parent1.copy()
    enfant2 = parent2.copy()
    # Logique pour croiser les parents et générer des enfants
    return enfant1, enfant2

def mutation(individu, clients, temps_max):
    for tournee in individu:
        if len(tournee['tournee']) > 2:  # Assurez-vous qu'il y a des clients à muter
            index = random.randint(1, len(tournee['tournee']) - 2)  # Ne pas muter 'd' ou 'a'
            client_id = tournee['tournee'].pop(index)
            clients_restants = [c for c in clients if c['id'] == client_id]
            if clients_restants and random.random() < 0.5:
                # Insérer le client à une nouvelle position aléatoire entre 'd' et 'a'
                new_index = random.randint(1, len(tournee['tournee']) - 1)
                tournee['tournee'].insert(new_index, clients_restants[0]['id'])
                
            # Recalculer le temps et le profit de la tournée après mutation
            temps_courant = 0
            profit_total = 0
            for client_id in tournee['tournee'][1:-1]:  # Ignorer 'd' et 'a'
                client = next(c for c in clients if c['id'] == client_id)
                temps_courant += client['time']
                profit_total += client['profit']
                if temps_courant > temps_max:
                    break
            tournee['profit'] = profit_total

def methode_genetique(clients, temps_max, nombre_de_vehicules, population_size=20, generations=50, mutation_rate=0.2):
    population = [heuristique_gloutonne(clients, temps_max, nombre_de_vehicules) for _ in range(population_size)]
    meilleure_solution = max(population, key=lambda x: sum([tournee['profit'] for tournee in x]))

    for _ in range(generations):
        nouvelle_population = []

        # Sélection et croisement
        for _ in range(population_size // 2):
            parent1, parent2 = random.sample(population, 2)
            enfant1, enfant2 = croisement(parent1, parent2)
            nouvelle_population.extend([enfant1, enfant2])

        # Mutation
        for individu in nouvelle_population:
            if random.random() < mutation_rate:
                mutation(individu, clients, temps_max)

        # Mise à jour de la population
        population = nouvelle_population
        meilleure_solution = max(population, key=lambda x: sum([tournee['profit'] for tournee in x]))

    return meilleure_solution

def main():
     # Déclaration des sommets
    nb_sommets = int(input("Entrez le nombre de sommets : ")) # nombre de sommets 7
    sommets = [0] * (nb_sommets)
    sommets[0]= "d"
    sommets[-1]= "a"
    # Remplissage de la liste des sommets entre d et a
    for i in range(1, nb_sommets-1):
        sommets[i] = i
    # Affichage des sommets
    print("Sommets :", sommets)

    # Déclaration des profits des clients hors dépôt et arrivée
    # profits = [0] + [random.randint(1, 10) for _ in range(nb_sommets-2)] + [0]

    profits = [0, 10, 15, 20, 30, 25, 0]
    
    # Déclaration de la matrice des distances avec diagonales à zéro
    """distances = [[random.randint(1, 10) if i != j else 0 for j in range(nb_sommets)] for i in range(nb_sommets)]
    print("Matrice des distances :", distances)"""

    distances = {
    'd': {1: 3, 2: 6, 3: 7, 4: 8, 5: 9, 6: 10, 'a': 10},
    1: {1: 0, 2: 4, 3: 5, 4: 6, 5: 7, 6: 8, 'a': 8},
    2: {1: 3, 2: 0, 3: 3, 4: 4, 5: 5, 6: 6, 'a': 6},
    3: {1: 6, 2: 4, 3: 0, 4: 5, 5: 6, 6: 7, 'a': 7},
    4: {1: 7, 2: 5, 3: 3, 4: 0, 5: 8, 6: 9, 'a': 9},
    5: {1: 8, 2: 6, 3: 4, 4: 5, 5: 0, 6: 10, 'a': 10},
    6: {1: 9, 2: 7, 3: 5, 4: 6, 5: 8, 6: 0, 'a': 10},
    'a': {1: 10, 2: 8, 3: 6, 4: 7, 5: 9, 6: 10, 'a': 0}
    }
    
    # Déclaration de la limite de la capacité
    cout_max = int(input("Entrez le coût max : "))
    nombre_de_vehicules = int(input("Entrez le nombre de véhicules : "))

    # Génération aléatoire des clients
    clients = [{'id': sommets[i], 'profit': profits[i]} for i in range(1, nb_sommets-1)]
    
    # Initialisation des tournées avec les différentes méthodes
    solution_gloutonne = heuristique_gloutonne(clients, sommets, distances, cout_max, nombre_de_vehicules)

    print("Solution gloutonne : \n")    

    for solution in solution_gloutonne:
        print("\nVéhicule : ", solution['vehicule'])
        for tournee in solution['tournee']:
            print("ID :", tournee['id'], end=" ")
            print("Cout :", tournee['cout'], end=" ")
            print("Profit :", tournee['profit'], end=" ")
            print("\n")
        print("Cout total :", solution['distance'])
        print("\nProfit total :", solution['profit'])


    """solution_tabou = heuristique_tabou(clients, cout_max, nombre_de_vehicules)
    print("Solution tabou :", solution_tabou)"""

if __name__ == "__main__":
    main()
